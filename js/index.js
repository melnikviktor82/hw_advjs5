// Завдання

//- Створити сторінку, яка імітує стрічку новин соціальної мережі [Twitter](https://twitter.com/).

// #### Технічні вимоги:

//  - При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
//    - `https://ajax.test-danit.com/api/json/users`
//    - `https://ajax.test-danit.com/api/json/posts`
//  - Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
//  - Кожна публікація має бути відображена у вигляді картки (приклад в папці), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
//  - На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки.
// При натисканні на неї необхідно надіслати DELETE запит на адресу `https://ajax.test-danit.com/api/json/posts/${postId}`. Після отримання підтвердження із сервера
// (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
//  - Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти [тут](https://ajax.test-danit.com/api-pages/jsonplaceholder.html).
//  - Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
//  - Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас `Card`. При необхідності ви можете додавати також інші класи.

"use strict";
const JsonplaceholderAPI = "https://ajax.test-danit.com/api/json/";
const root = document.querySelector("#root");

class Request {
  constructor(url) {
    this.url = url;
  }
  get(entity) {
    return fetch(this.url + entity, {
      method: "GET",
    }).then((responce) => responce.json());
  }
  delete(entity, id) {
    return fetch(`${this.url}${entity}/${id}`, {
      method: "DELETE",
    }).then((responce) => responce);
  }
}

class TwitterPosts {
  constructor(url, target) {
    this.request = new Request(url);
    this.target = target;
  }

  render() {
    Promise.all([this.request.get("users"), this.request.get("posts")])
      .then((jsonResponse) => {
        const usersArr = jsonResponse[0];
        const postsArr = jsonResponse[1];

        postsArr.forEach(({ id, userId, title, body }) => {
          const { name, email } = usersArr.find(({ id, name, email }) => {
            if (userId === id) {
              return { name, email };
            }
          });
          const newCard = new Card(
            name,
            email,
            title,
            body,
            id,
            userId,
            this.request
          );

          this.target.append(newCard.render());
        });
      })
      .catch((error) => alert(error.message));
  }
}

class Card {
  constructor(name, email, title, body, id, userId, request) {
    this.name = name;
    this.email = email;
    this.postTitle = title;
    this.postContent = body;
    this.id = id;
    this.userId = userId;
    this.request = request;
  }

  render() {
    const userPost = document.createElement("div");
    userPost.id = this.id;
    userPost.classList.add("userPost");

    const flexDiv = document.createElement("div");
    flexDiv.classList.add("flexDiv");

    const userName = document.createElement("h2");
    userName.textContent = `${this.name}`;

    const userMail = document.createElement("a");
    userMail.textContent = `${this.email}`;
    userMail.href = `mailto:${this.email}`;

    const postTitle = document.createElement("h3");
    postTitle.textContent = `${this.postTitle}`;

    const postBody = document.createElement("p");
    postBody.textContent = `${this.postContent}`;

    const postDeleteBtn = document.createElement("button");
    postDeleteBtn.textContent = "DELETE POST";
    postDeleteBtn.addEventListener("click", (event) => {
      event.preventDefault();
      this.request
        .delete("posts", this.id)
        .then((response) => {
          if (response.ok) {
            document.getElementById(this.id).remove();
            alert("Post successfully deleted!");
          }
        })
        .catch((error) => alert(error.message));
    });
    flexDiv.append(userName, userMail)
    userPost.append(flexDiv, postTitle, postBody, postDeleteBtn);
    return userPost;
  }
}

const newTwitterPosts = new TwitterPosts(JsonplaceholderAPI, root);
newTwitterPosts.render();
